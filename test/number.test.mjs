import chai from 'chai';

import { isWithin } from '../esm/number.js';

const expect = chai.expect;

describe('Number utils', () => {
  describe('isWithin', () => {
    const range = [11, 50];
    const numberWithinRange = 36;
    const numberOutsideRangeLeft = 0;
    const numberOutsideRangeRight = 69;
    const numberStr = '33';

    it('should return true if the number is within given range', async () => {
      expect(isWithin(numberWithinRange, ...range)).to.be.true;
    });

    it('should return true if the number is on one of the boundaries of given range', async () => {
      expect(isWithin(range[0], ...range)).to.be.true;
      expect(isWithin(range[1], ...range)).to.be.true;
    });

    it('should return false if the number is outside the given range', async () => {
      expect(isWithin(numberOutsideRangeLeft, ...range)).to.be.false;
      expect(isWithin(numberOutsideRangeRight, ...range)).to.be.false;
    });

    it('should throw TypeError if the argument is not a number', async () => {
      expect(() => isWithin(numberStr, ...range))
        .to.throw(TypeError);
    });
  });
});