import chai from 'chai';

import { getFullKeyPath, isObjectEmpty } from '../esm/object.js';

const expect = chai.expect;

describe('Object utils', () => {
  describe('getFullKeyPath', () => {
    const testObj = {
      a: {
        a: 'abc',
        b: {
          a: 'def',
          b: 'ghi'
        },
        c: ['jkl', ['mno']]
      },
      b: 'pqr',
      c: 'stu',
      d: 'abc'
    };

    it('should return correct path to value which is a string property', () => {
      expect(getFullKeyPath(testObj, 'ghi'))
        .to.be.a('string').eq('a.b.b');
    });

    it('should return correct path to value which contains in an array', () => {
      expect(getFullKeyPath(testObj, 'mno'))
        .to.be.a('string').eq('a.c#1#0');
    });

    it('should return correct path to first found value if object has multiple entries with same value', () => {
      expect(getFullKeyPath(testObj, 'abc'))
        .to.be.a('string').eq('a.a');
    });

    it('should return null if object doesn\'t contain provided value', () => {
      expect(getFullKeyPath(testObj, 'xyz'))
        .to.be.null;
    });
  });

  describe('isObjectEmpty', () => {
    it ('should return true if given object is empty', () => {
      expect(isObjectEmpty({})).to.be.true;
    });

    it ('should return false if given object is not empty', () => {
      expect(isObjectEmpty({ test: 123 })).to.be.false;
    });

    it ('should return false if given value is not an object', () => {
      expect(isObjectEmpty('{ test: 123 }')).to.be.false;
    });
  });
});