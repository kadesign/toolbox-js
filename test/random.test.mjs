import chai from 'chai';

import { getRandomIndex, getRandomItem, getRandomNumberBetween } from '../esm/random.js';

const expect = chai.expect;

describe('Functions returning random values', () => {
  describe('getRandomIndex', () => {
    it('should return valid index of array', () => {
      const arr = [1, 2, 3, 4, 5];

      expect(getRandomIndex(arr)).to.be.a('number').within(0, arr.length - 1);
    });

    it('should return null if array is empty', () => {
      expect(getRandomIndex([])).to.be.null;
    });

    it('should throw TypeError if argument is not an array', () => {
      expect(() => getRandomIndex('[]')).to.throw(TypeError, 'Argument value is not an array');
    });
  });

  describe('getRandomItem', () => {
    it('should return existing item from an array', () => {
      const arr = [1, 2, 3, 4, 5];

      const item = getRandomItem(arr);
      expect(item).not.to.be.null;
      expect(arr).to.contain(item);
    });

    it('should return null if array is empty', () => {
      expect(getRandomItem([])).to.be.null;
    });

    it('should throw TypeError if argument is not an array', () => {
      expect(() => getRandomItem('[]')).to.throw(TypeError, 'Argument value is not an array');
    });
  });

  describe('getRandomNumberBetween', () => {
    it(`should return number between given a and b`, () => {
      const a = Math.floor(Math.random() * 10);
      const b = 11 + Math.floor(Math.random() * 10);

      expect(getRandomNumberBetween(a, b)).to.be.a('number').within(a, b);
    });

    it(`should return the same number if both a and b are equal`, () => {
      const a = Math.floor(Math.random() * 10);

      expect(getRandomNumberBetween(a, a)).to.be.a('number').eq(a);
    });

    it(`should throw TypeError if one or both arguments are not a number`, () => {
      expect(() => getRandomNumberBetween('1', '2')).to.throw(TypeError, 'Both arguments should be numbers');
    });

    it(`should throw RangeError if first argument is greater than second argument`, () => {
      const a = 11 + Math.floor(Math.random() * 10);
      const b = Math.floor(Math.random() * 10);

      expect(() => getRandomNumberBetween(a, b)).to.throw(RangeError, `Invalid range: ${a} is greater than ${b}`);
    });
  });
});
