import chai from 'chai';

import { shuffleArray, orderBy } from '../esm/array.js';

const expect = chai.expect;

describe('Array utils', () => {
  describe('shuffleArray', () => {
    const original = [1, 3, 5, 7, 9];
    let testData;

    beforeEach(() => {
      testData = original.slice();
    })

    it('should return shuffled array so it won\'t be equal to the original one', () => {
      shuffleArray(testData);

      expect(testData)
        .to.be.an('array')
        .and.to.have.members(original);
    });

    it('should throw TypeError if argument is not an array', () => {
      expect(() => shuffleArray('[1, 3, 5, 7, 9]')).to.throw(TypeError, 'Argument value is not an array');
    });
  });

  describe('orderBy', () => {
    // TODO
  });
});