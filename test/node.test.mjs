import chai from 'chai';

import { checkEnvVariables } from '../esm/node.js';

const expect = chai.expect;

describe('Node-related utils', () => {
  describe('checkEnvVariables', () => {
    const validVars = ['TEST_VAR_1', 'TEST_VAR_3', 'TEST_VAR_4'];
    const invalidVars = ['TEST_VAR_2', 'TEST_VAR_5'];

    before(() => {
      validVars.forEach(variable => {
        process.env[variable] = 'test_value';
      });
    });

    it('should not fail if all passed variables are set', async () => {
      expect(() => checkEnvVariables(...validVars))
        .to.not.throw();
    });

    it('should throw ReferenceError with correct message if one variable is not set', async () => {
      expect(() => checkEnvVariables(...validVars, invalidVars[0]))
        .to.throw(ReferenceError, `Following environment variables are not set: ${invalidVars[0]}`);
    });

    it('should throw ReferenceError with correct message if more than one variable is not set', async () => {
      expect(() => checkEnvVariables(...invalidVars, ...validVars))
        .to.throw(ReferenceError, `Following environment variables are not set: ${invalidVars.join(', ')}`);
    });
  });
});