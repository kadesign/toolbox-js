import chai from 'chai';

import { escapeUnderscore, formatFileSize } from '../esm/format.js';

const expect = chai.expect;

describe('String formatting', () => {
  describe('escapeUnderscore', () => {
    it(`should return a string with one or more escaped underscore`, () => {
      const testData = [
        ['user_name1', 'user\\_name1'],
        ['cool_daddy_007', 'cool\\_daddy\\_007']
      ];

      testData.forEach(username => {
        expect(escapeUnderscore(username[0])).to.be.a('string').eq(username[1]);
      });
    });

    it(`should return same string if it has no underscores`, () => {
      expect(escapeUnderscore('username123')).to.be.a('string').eq('username123');
    });
  });

  describe('formatFileSize', () => {
    // TODO
  });
});