# Changelog

## 0.1.4
* Add function `getRandomItem(array)` to `index.js`

## 0.1.3
* Add new function `getRandomItem(array)` to module "random"

## 0.1.2
* Add new module "number" with function `isWithin(number, min, max)`

## 0.1.1
* Restructure code and simplify build process

## 0.1.0
Initial release
