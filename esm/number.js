/**
 * Check if a number is within given range.
 *
 * @example
 * isWithin(31, 20, 100); // returns true
 * isWithin(31, 40, 100); // returns false
 *
 * @param  {number} number Number to check
 * @param  {number} min    Min boundary of checked range
 * @param  {number} max    Max boundary of checked range
 *
 * @returns {boolean} True if a number is within given range
 *
 * @throws {TypeError} One or more arguments are not a number
 */
export const isWithin = (number, min, max) => {
  if (typeof number !== 'number' || typeof min !== 'number' || typeof max !== 'number') {
    throw new TypeError('One or more arguments are not a number');
  }

  return number >= min && number <= max;
};