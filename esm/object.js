/**
 * Search value in object and return full path to the first found key with this value.
 *
 * Nested properties are split with '.', index of element inside an array is prefixed with '#'
 *
 * @example
 * const foo = {
 *   bar: {
 *     baz: 100,
 *     xyz: 200
 *   },
 *   baz: [300, 400, 500],
 *   xyz: 400
 * };
 *
 * getFullKeyPath(foo, 100); // returns 'foo.bar.baz'
 * getFullKeyPath(foo, 400); // returns 'foo.baz#1'
 *
 * @param {object} obj   Object where value given value should be found
 * @param {*}      value Desired value
 *
 * @return {?string} Full path to the first found key in the object or null if such value doesn't exist in the object
 */
export const getFullKeyPath = (obj, value) => {
  let path = '';

  Object.keys(obj).some(key => {
    if (Array.isArray(obj[key]) || typeof obj[key] === 'object' && obj[key] !== null) {
      const buffer = getFullKeyPath(obj[key], value);
      if (buffer) {
        path = key;
        if (Array.isArray(obj[key])) {
          path += `#${buffer}`;
        } else {
          path += `.${buffer}`;
        }
        return path;
      }
    } else {
      if (obj[key] === value) {
        path = key;
        return path;
      }
    }
  });

  return path.length !== 0 ? path : null;
};

/**
 * Check whether given object is empty.
 *
 * @example
 * const objA = {foo: 'bar'}
 * isObjectEmpty(objA); // returns false
 *
 * const objB = {};
 * isObjectEmpty(objB); // returns true
 *
 * @param {object} obj Object for checking
 *
 * @return {boolean} True if given value is empty object
 */
export const isObjectEmpty = (obj) => {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
};
