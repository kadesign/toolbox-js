/**
 * Check if a set of environment variables is configured.
 *
 * @example
 * // Assuming environment variables VAR_1 and VAR_3 are set
 * // Returns void
 * checkEnvVariables('VAR_1', 'VAR_3');
 * // Throws error "Following environment variables are not set: VAR_2"
 * checkEnvVariables('VAR_2', 'VAR_3');
 *
 * @param  {...string} variableNames Names of environment variables to check
 *
 * @throws {ReferenceError} One or more environment variables are not set
 */
export const checkEnvVariables = (...variableNames) => {
  const missingEnvVars = [];
  variableNames.forEach(envVar => {
    if (!Object.keys(process.env).includes(envVar)) missingEnvVars.push(envVar);
  });
  if (missingEnvVars.length > 0)
    throw new ReferenceError(`Following environment variables are not set: ${missingEnvVars.join(', ')}`);
};
