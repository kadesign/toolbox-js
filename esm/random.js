/**
 * Generate random index of given array.
 *
 * @example
 * const arr = [1, 2, 3, 4, 5]; // arr.length = 5
 * getRandomIndex(arr);  // Returns random value from 0 to 4
 *
 * @param {Array<*>} array Array to generate random index from
 *
 * @return {?number} Random array index or null if given array is empty
 * @throws {TypeError} Argument value is not an array
 */
export const getRandomIndex = (array) => {
  if (!Array.isArray(array)) throw new TypeError("Argument value is not an array");

  if (array.length === 0) return null;
  return Math.floor(Math.random() * array.length);
};

/**
 * Get random item from given array.
 *
 * @example
 * const arr = [1, 2, 3, 4, 5];
 * getRandomItem(arr); // Returns any item from the array
 *
 * @param {Array<*>} array Array to return random item from
 *
 * @returns {*|null} Random array item or null if given array is empty
 * @throws {TypeError} Argument value is not an array
 */
export const getRandomItem = (array) => {
  if (!Array.isArray(array)) throw new TypeError("Argument value is not an array");

  if (array.length > 0) {
    const index = Math.floor(Math.random() * array.length);
    return array[index];
  } else {
    return null;
  }
}

/**
 * Generate random number from given range including boundary values.
 *
 * @example
 * getRandomNumberBetween(13, 28); // Returns random value between 13 and 28 including them
 *
 * @param {number} a Left border of the range
 * @param {number} b Right border of the range
 *
 * @return {number}     Random number from provided range
 * @throws {TypeError}  One or both arguments' values are not a number
 * @throws {RangeError} First argument is greater than second argument
 */
export const getRandomNumberBetween = (a, b) => {
  if (typeof a !== 'number' || typeof b !== 'number') throw new TypeError('Both arguments should be numbers');
  if (a > b) throw new RangeError(`Invalid range: ${a} is greater than ${b}`);

  if (a === b) return a;
  return a + Math.floor(Math.random() * (b - a + 1));
};
