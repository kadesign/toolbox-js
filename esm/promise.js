/**
 * Promise wrapper
 *
 * @example
 * const result = await to(promiseFunc(args));
 * // Returns { result: 'some result' } if resolved
 * // or { error: SomeError } if rejected
 *
 * @param {Promise} promise Promise to be resolved
 *
 * @return {Promise<{result: *} | {error: *}>} Result from promise or execution error
 */
export const to = (promise) => {
  return promise
    .then(data => ({ result: data }))
    .catch(err => ({ error: err }));
};
