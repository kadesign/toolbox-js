/**
 * Shuffle array elements in place. Uses Fisher-Yates shuffle algorithm.
 *
 * Order of elements in shuffled array can be the same as before shuffling.
 *
 * @see {@link https://stackoverflow.com/a/6274381 Source (stackoverflow.com)}
 *
 * @example
 * const arr = [1, 2, 3, 4, 5];
 * shuffleArray(arr); // shuffles array in place, e.g. [3, 1, 2, 5, 4]
 *
 * @param {Array<*>} array Array to be shuffled
 *
 * @throws {TypeError} Argument value is not an array
 */
export const shuffleArray = (array) => {
  if (!Array.isArray(array)) throw new TypeError("Argument value is not an array");

  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
};

/**
 * Compare function to sort an array of objects by multiple keys. All objects in the array must contain given keys.
 *
 * @see {@link https://github.com/you-dont-need/You-Dont-Need-Lodash-Underscore/issues/280#issuecomment-690604745 Source (github.com)}
 *
 * @example
 * const arr = [
 *   { a: 12, b: 2 },
 *   { a: 11, b: 3 },
 *   { a: 12, b: 1 }
 * ];
 * // Sort array by 'a' in descending order and by 'b' in ascending order
 * arr.sort(orderBy( [ 'a', 'b' ], [ 'desc', 'asc' ] ));
 * // Array after sorting:
 * // [
 * //   { a: 12, b: 1 },
 * //   { a: 12, b: 2 },
 * //   { a: 11, b: 3 }
 * // ]
 *
 * @param keys   {Array<string>} Keys to sort by
 * @param orders {Array<string>} Sorting directions for corresponding key. Possible values are 'asc', 'desc'
 *
 * @returns {function(): number}
 *
 * @throws {Error} Unsupported order is used for at least one key
 * @throws {Error} At least one object in array does not contain one of given keys
 */
export const orderBy = (keys, orders) => {
  const sortBy = (key, cb) => {
    if (!cb) cb = () => 0;
    return (a, b) => {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        throw new Error(`At least one object in array does not contain key ${key}`);
      }

      return (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : cb(a, b));
    }
  };

  const sortByDesc = (key, cb) => {
    if (!cb) cb = () => 0;
    return (b, a) => {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        throw new Error(`One or more objects in array do not contain key ${key}`);
      }
      return (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : cb(b, a));
    }
  };

  let cb = () => 0;
  keys.reverse();
  orders.reverse();
  for (const [i, key] of keys.entries()) {
    const order = orders[i];
    if (order === 'asc') {
      cb = sortBy(key, cb);
    } else if (order === 'desc') {
      cb = sortByDesc(key, cb);
    } else {
      throw new Error(`Unsupported order "${order}"`);
    }
  }
  return cb;
};
