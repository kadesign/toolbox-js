import gulp from 'gulp';
import esmToCjs from 'gulp-esm-to-cjs';
import del from 'del';

const ESM_DIR = 'esm';
const CJS_DIR = 'cjs';

gulp.task('convert-to-cjs', () => {
  const src = `${ESM_DIR}/*.js`;
  const dest = `${CJS_DIR}`;
  const options = {
    quote: 'single',
    lenIdentifier: 23
  };
  return gulp
    .src(src)
    .pipe(esmToCjs(options))
    .pipe(gulp.dest(dest));
});

gulp.task('clean', () => {
  return del([ `${CJS_DIR}/*.js`, `!${CJS_DIR}/package.json` ]);
});

gulp.task('default', gulp.series('clean', 'convert-to-cjs'));