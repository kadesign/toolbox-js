# toolbox-js
[![Go to NPM package](https://img.shields.io/npm/v/@kadesign/toolbox-js?label=version)](https://www.npmjs.com/package/@kadesign/toolbox-js)
[![Pipeline status](https://gitlab.com/kadesign/toolbox-js/badges/master/pipeline.svg)](https://gitlab.com/kadesign/toolbox-js/-/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/kadesign/toolbox-js/badge.svg?branch=master)](https://coveralls.io/gitlab/kadesign/toolbox-js?branch=master)
![Maintenance status](https://img.shields.io/maintenance/yes/2024)

Bunch of small utility functions

# Usage

## Installation
```shell
npm install @kadesign/toolbox-js
```

## Import
### From global scope
```js
// ESM
import * as toolbox from '@kadesign/toolbox-js';
import { isObjectEmpty } from '@kadesign/toolbox-js';

// CommonJS
const toolbox = require('@kadesign/toolbox-js');
const { isObjectEmpty } = require('@kadesign/toolbox-js');
```
### From module
```js
// ESM
import * as obj from '@kadesign/toolbox-js/esm/object.js';
import { isObjectEmpty } from '@kadesign/toolbox-js/esm/object.js';

// CommonJS
const obj = require('@kadesign/toolbox-js/cjs/object.js');
const { isObjectEmpty } = require('@kadesign/toolbox-js/cjs/object.js');
```

# Functions

<table>
<thead>
    <tr>
        <th>Module</th>
        <th>Function</th>
        <th>Short description</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td rowspan="2"><b>array</b></td>
        <td><code>shuffleArray(array: any[]): void</code></td>
        <td>Shuffle array elements in place. Uses Fisher-Yates shuffle algorithm.</td>
    </tr>
    <tr>
        <td><code>orderBy(keys: string[], orders: string[]): function(): number</code></td>
        <td>Compare function to sort an array of objects by multiple keys. All objects in the array must contain given keys.</td>
    </tr>
    <tr>
        <td rowspan="2"><b>format</b></td>
        <td><code>escapeUnderscore(str: string): string</code></td>
        <td>Escape underscores in given string.</td>
    </tr>
    <tr>
        <td><code>formatFileSize(bytes: number, si: ?boolean = false, dp: ?number = 1): string</code></td>
        <td>Format bytes as human-readable text.</td>
    </tr>
    <tr>
        <td><b>node</b></td>
        <td><code>checkEnvVariables(...variableNames: string[]): void</code></td>
        <td>Check if a set of environment variables is configured.</td>
    </tr>
    <tr>
        <td><b>number</b></td>
        <td><code>isWithin(number: number, min: number, max: number): boolean</code></td>
        <td>Check if a number is within given range.</td>
    </tr>
    <tr>
        <td rowspan="2"><b>object</b></td>
        <td><code>getFullKeyPath(obj: object, value: any): ?string</code></td>
        <td>Search value in object and return full path to the first found key with this value.</td>
    </tr>
    <tr>
        <td><code>isObjectEmpty(obj: object): boolean</code></td>
        <td>Check whether given object is empty.</td>
    </tr>
    <tr>
        <td><b>promise</b></td>
        <td><code>to(promise: Promise):  Promise<{result: any} | {error: any}></code></td>
        <td>Promise wrapper.</td>
    </tr>
    <tr>
        <td rowspan="3"><b>random</b></td>
        <td><code>getRandomIndex(array: any[]): ?number</code></td>
        <td>Generate random index of given array.</td>
    </tr>
    <tr>
        <td><code>getRandomItem(array: any[]): ?any</code></td>
        <td>Get random item from given array.</td>
    </tr>
    <tr>
        <td><code>getRandomNumberBetween(a: number, b: number): number</code></td>
        <td>Generate random number from given range including boundary values.</td>
    </tr>
</tbody>
</table>
